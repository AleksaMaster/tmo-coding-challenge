/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';

import { environment } from './environments/environment';

const apiURL = environment.apiURL;
const apiKey = environment.apiKey;

const init = async () => {
    const server = new Server({
        port: 3333,
        host: 'localhost',
        routes: {
            cors: true
        }
    });

    await server.register({ plugin: require('h2o2') });

    server.route({
        method: 'GET',
        path: '/beta/stock/{symbol}/chart/{period}',
        handler: {
            proxy: {
                uri: `${apiURL}/beta/stock/{symbol}/chart/{period}?token=${apiKey}`
            }
        },
        options: {
            cache: {
                expiresIn: 30 * 1000,
                privacy: 'private'
            }
        }
    });
    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
    console.log(err);
    process.exit(1);
});

init();
