// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    apiKey: 'pk_ba29d8b3694f4e46a4bc3e781b1adf47',
    apiURL: 'https://cloud.iexapis.com'
};
