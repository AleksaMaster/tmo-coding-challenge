# T-Mobile Coding Challenge

### Important! Read this First !

Do **not** submit a pull request to this repository.  You PR wil be rejected and your submission ignored.

To _properly_ submit a coding challenge you must:

1. fork this repository
2. make the necessary changes
3. push changes to your forked origin 
4. send address of your fork to t-mobile.

We will review your fork online before and during your interview.


# Stocks coding challenge

## How to run the application

There are two apps: `stocks` and `stocks-api`.

- `stocks` is the front-end. It uses Angular 7 and Material. You can run this using `yarn serve:stocks`
- `stocks-api` uses Hapi and has a very minimal implementation. You can start the API server with `yarn serve:stocks-api`

A proxy has been set up in `stocks` to proxy calls to `locahost:3333` which is the port that the Hapi server listens on.

> You need to register for a token here: https://iexcloud.io/cloud-login#/register/ Use this token in the `environment.ts` file for the `stocks` app.

> The charting library is the Google charts API: https://developers.google.com/chart/

## Problem statement

[Original problem statement](https://bitbucket.org/kburson3/developer-puzzles/src/3fb1841175cd567a63abfbe18c08e4d2a734c2e9/puzzles/web-api/stock-broker.md)

### Task 1

Please provide a short code review of the base `master` branch:

1. What is done well?
	- Application is done in a very modular way, with lazy loading of features and also clear separation of features and shared ui widgets, where feature components are 
	  smart (container) components and widgets are presentational components used only for presentation of data without any logic
	- Using of Facade classes, which encapsulates work with underlying store. That means that client of Facade is not aware of store at all, and if we want to replace store
	  with something else in the future we would not need to change client code that uses Facade class

2. What would you change?
	- Create a separate module for defining feature routes instead of using app.module
	- Create a barrel material module which should include all material modules which are used in application, so importing material into feature modules becomes easier
	- I would change ChartComponent to get quotes list as Input instead of Observable. This way we can use DetectionStrategy.OnPush. Every presentational component like this one
	  should be done in that way to work with DetectionStrategy.OnPush strategy. Fix for this done on feat_task1 branch.
	- ChartComponent should receive char options as Input (optional), so we can override default options if needed. Current implementation has hardcoded options inside
	  ngOnInit in ChartComponent
	- 
	
3. Are there any code smells or problematic implementations?
	- Chart is not shown because of wrong ngIf condition
	- I can see 'any' being used for some variables as a type which should be avoided. Example: chartData:any - Interface or class should be created here
	

> Make a PR to fix at least one of the issues that you identify

### Task 2

```
Business requirement: As a user I should be able to type into
the symbol field and make a valid time-frame selection so that
the graph is refreshed automatically without needing to click a button.
```

_**Make a PR from the branch `feat_stock_typeahead` to `master` and provide a code review on this PR**_

> Add comments to the PR. Focus on all items that you can see - this is a hypothetical example but let's treat it as a critical application. Then present these changes as another commit on the PR.

### Task 3

```
Business requirement: As a user I want to choose custom dates
so that I can view the trends within a specific period of time.
```

_**Implement this feature and make a PR from the branch `feat_custom_dates` to `master`.**_

> Use the material date-picker component

> We need two date-pickers: "from" and "to". The date-pickers should not allow selection of dates after the current day. "to" cannot be before "from" (selecting an invalid range should make both dates the same value)

### Task 4

```
Technical requirement: the server `stocks-api` should be used as a proxy
to make calls. Calls should be cached in memory to avoid querying for the
same data. If a query is not in cache we should call-through to the API.
```

_**Implement the solution and make a PR from the branch `feat_proxy_server` to `master`**_

> It is important to get the implementation working before trying to organize and clean it up.
