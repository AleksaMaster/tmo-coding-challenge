import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { debounceTime, distinctUntilChanged, tap, filter } from 'rxjs/operators';
import { noop, Subscription } from 'rxjs';

@Component({
    selector: 'coding-challenge-stocks',
    templateUrl: './stocks.component.html',
    styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit, OnDestroy {
    stockPickerForm: FormGroup;

    quotes$ = this.priceQuery.priceQueries$;

    subscriptionSymbol: Subscription;
    subscriptionPeriod: Subscription;

    maxDate = new Date();

    constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
        this.stockPickerForm = fb.group({
            symbol: [null, Validators.required],
            periodFrom: [null],
            periodTo: [null]
        });
    }

    get maxFromDate() {
        return !!this.stockPickerForm.value.periodTo ? this.stockPickerForm.value.periodTo : this.maxDate;
    }

    ngOnInit() {
        this.subscriptionSymbol = this.stockPickerForm.get('symbol').valueChanges.pipe(
            debounceTime(400),
            distinctUntilChanged()
        ).subscribe(() => this.fetchQuote());
    }

    ngOnDestroy() {
        this.subscriptionSymbol.unsubscribe();
        this.subscriptionPeriod.unsubscribe();
    }

    fetchQuote() {
        if (this.stockPickerForm.valid) {
            const { symbol, periodFrom, periodTo } = this.stockPickerForm.value;
            this.priceQuery.fetchQuoteByDate(symbol, periodFrom, periodTo);
        }
    }

    onPeriodChange() {
        const periodFrom = this.stockPickerForm.value.periodFrom;
        const periodTo = this.stockPickerForm.value.periodTo;

        if (!!periodFrom && !!periodTo && periodFrom > periodTo) {
            this.stockPickerForm.get('periodFrom').setValue(periodTo);
        }

        if (!!periodFrom && !!periodTo) {
            this.fetchQuote();
        }
    }
}
